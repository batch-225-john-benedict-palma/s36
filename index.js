// Initialization, Connection
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();


const taskRoute = require ("./routes/taskRoute");


const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended: true }))

mongoose.connect(`mongodb+srv://johnbenedictpalma:09224300177@cluster0.zxskweo.mongodb.net/?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."));

//Add the task routes
//allows all the task created in the "taskRoute.js" file to use "/tasks" route

app.use("/tasks", taskRoute);



// Port connect
app.listen(port, () => console.log(`Server running at localhost: ${port}`));