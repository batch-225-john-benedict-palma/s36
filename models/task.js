// Create the Schema

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

//"module.exports" is the way for node.js to treat this value as "package" that cab be used by other file

module.exports = mongoose.model("Tasks", taskSchema);