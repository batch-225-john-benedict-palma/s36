//Contains all the endpoint for our application

// We need to use express' Router() function to achieve this;
const express = require("express");

//Creates a Router instance that function as middleware and routing system
//Allows access to HTTP method middleware that makes it easier to create routes for our application
const router = express.Router();

const taskController = require("../controller/taskController");

/*
	Syntax: localhost:3001/tasks/getinfo
*/

//SECTION: Routes

//Route to get all the task
router.get("/getinfo", (req, res) =>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));


})

//Route to create a new task

router.post("/create", (req, res) => {


	//If info will be comming form the client side, commonly from forms, the data can be accessed from the request "body" props.
	taskController.createTask(req.body).then(result => res.send(result));
} )


//Route to delete a task
//the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
//The word that comes after the colon (:) symbol will be the name of the URL param // ":id" os a wildcard where you can put any value, it crates a link b/w "id" param in the URL and the value provided in the URL

/*
ex:
localhost:3001/tasks/delete/63c68fff2ad92127f739277f

the 63c68fff2ad92127f739277f is assigned to the "id" parameter in the URL
*/

router.delete("/delete/:id" , (req, res ) => {


	// URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	// If information will be coming from the URL, the data can be accessed from the request "params" property
	taskController.deleteTask(req.params.id).then(result => res.send(result))
})

//Route to update a task

router.put("/update/:id",  (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
}) 

//Use "module.exports" to export the router object to usein the "app.js"




module.exports = router;


// Activity


router.get("/:id", (req, res) =>{

	taskController.getAllTasksNew(req.params.id).then(resultFromController => res.send(resultFromController));


})


router.put("/update2/:id",  (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
}) 

