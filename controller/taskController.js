//Controller contains a functions and business logic of our express JS. Meaning all the operation it can do will be placed in this file.

const Task = require("../models/task")

module.exports.getAllTasks = () => {

	//the 'Return' statement. returms the result of the Mongoose method
	//Then "then" method is used to wait for the Mongoose method to finish b4 sending the result back to the route

	return Task.find({}).then(result => {

		return result;

	})

}

module.exports.createTask = (requestBody) => {

	//create a task object based on the Mongoose model "Task"
	let newTask = new Task ({

		name : requestBody.name
	})

	return newTask.save().then((task, error) => {

		if (error) {
			console.log(error);
	
			//if an error was encountered, the "return" statement will prevent any other line or code within the same code block
			//the else statement will no longer evaluated
		return false;
		} else {

			return task
		}
	})

}

module.exports.deleteTask = (taskId) => {

	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field

	return Task.findByIdAndRemove(taskId).then((result, err) => {

		if (err) {
			console.log(err);
			return false;
		} else {
			return result
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err) {
			console.log(err)
			return false;
		} else {

			result.name = newContent.name;
return result.save().then((updated, saveErr) => {
			if (saveErr) {

				console.log(saveErr);
				return false;
			} else {

				return updated;
			}


		})

		}
	})
}

//Activity

module.exports.getAllTasksNew = (idFind) => {

	return Task.findById(idFind).then((result, err) =>{

		if(err) {
			console.log(err)
			return false;
		} else {
			return result;
		}

	})
}


module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if(err) {
			console.log(err)
			return false;
		} else {

			result.status = newContent.status;
return result.save().then((updated, saveErr) => {
			if (saveErr) {

				console.log(saveErr);
				return false;
			} else {

				return updated;
			}


		})

		}
	})
}
